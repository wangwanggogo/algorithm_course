#!/usr/bin/env python3
# *-* coding:utf-8 *-*
import sys
'''
给定如下有序集：s= {k1, k2,…, k5}   (k1<k2<…<k5) 
其对应元素的查找概率分布如下：
b(1-5) = {0.15,0.1,0.05,0.1,0.2}
a(0-5) = {0.05,0.1,0.05,0.05,0.05,0.1}

问题（1）：请描述求解最优二叉搜索树的动态规划算法
问题（2）：计算并填出最优值m(i,j)、子树概率wi,j和根节点标识xr(s[i][j]=r)的表格

'''

def optimal_binary_search_tree(a,b):
	m = {}
	root = {}
	w ={}
	for i in range(len(b)):
		w[i + 1] = {}
		m[i + 1] = {}
		root[i + 1] = {}
		w[i + 1][i] = a[i]
		m[i + 1][i] = a[i]
	for l in range(len(b)):
		for i in range(1,len(b) - l ):
			j = i + l
			w[i][j] = w[i][j - 1] + a[j] + b[j]
			m[i][j] = sys.float_info.max  #最大浮点数
			for k in range(i ,j + 1):
				t = m[i][k-1] + m[k + 1][j] + w[i][j]
				if t < m[i][j] :
					m[i][j] = t
					root[i][j] = k
	return m,w,root

def print_table(table):
    print("%s"%" ",end="")
    for i in range(0,len(table)):
        print("\t%5d"%i,end="")
    print()
    for i in range(1,len(table) + 1):
        print("%d"%i,end="")
        for j in range(0,len(table) + 1):
            if j in table[i]:
                print("\t%5.3f"%table[i][j],end="")
            else:
                print("\t%5s"%" ",end="")
        print()

def test_optimal_binary_search_tree():

	b = [0,0.15,0.1,0.05,0.1,0.2]
	a = [0.05,0.1,0.05,0.05,0.05,0.1]

	m,w,root = optimal_binary_search_tree(a,b)
	print("avg search count :%5.3f"%m[1][len(b)-1])
	print("最优值 m(i,j)")
	print_table(m)
	print("子树概率w(i,j)")
	print_table(w)
	print("根节点标识符")
	print_table(root)
	# print_table(m)
	# print(w)
if __name__ == '__main__':
	test_optimal_binary_search_tree()