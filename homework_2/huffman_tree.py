#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random
import copy
'''
设字符a, b, c, d, e, f, g在某文件中出现的频率依次是：（选做）
31%，12%，5%，2%，10%，18%，22%
请画出哈夫曼树，给出各字符编码，并计算平均码长

'''

def huffman_tree(chars):
	# chars.sort(key=lambda x:x['c'])
	if len(chars) == 1:
		return chars[0]
	min_c1 = min(chars,key = lambda x:x[1])
	chars.remove(min_c1)
	min_c2 = min(chars,key = lambda x:x[1])
	chars.remove(min_c2)
	while True:
		new_char = '%s'%random.randint(0,len(chars) * 2) #可把范围设置的更大些，减少重复的概率
		if new_char not in chars:
			break
	chars.append([new_char,round(min_c1[1] + min_c2[1],3),min_c1,min_c2]) #把构建的新节点加入之前节点当中，该节点左子树为min_c1,右字数为min_c2
	return huffman_tree(chars)


def test_huffman_tree():
	chars = [['a',0.31],['b',0.12],['c',0.05],['d',0.02],['e',0.1],['f',0.18],['g',0.22]]
	tree = huffman_tree(copy.deepcopy(chars))
	# print(tree)
	def trace_tree(tree,c=""):
		if len(tree) == 2:
			return {tree[0]:c},len(c) * tree[1]
		l_tree,l_length = trace_tree(tree[2],"%s0"%c)
		r_tree,r_length = trace_tree(tree[3],"%s1"%c)
		l_tree.update(r_tree)
		return l_tree,l_length + r_length
	codes,avg_length = trace_tree(tree)
	# print(codes)
	for c in codes:
		print("%s:%s"%(c,codes[c]))
	print("avg_length:%.3f"%avg_length)



if __name__ == '__main__':
	test_huffman_tree()

