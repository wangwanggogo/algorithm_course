#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''贪心算法
已知有若干活动的起止时间如下表所示
activity = [
	{'no':1,'s':2,'f':3},
	{'no':2,'s':2,'f':4},
	{'no':3,'s':3,'f':5},
	{'no':4,'s':4,'f':7},
	{'no':5,'s':6,'f':8},
	{'no':6,'s':7,'f':11},
	{'no':7,'s':9,'f':12},
	{'no':8,'s':10,'f':15},
	{'no':9,'s':13,'f':17}
]

问题：最多可以安排多少活动？为哪些？请编程实现输出

'''

def greedy_selector(activity):
	activity.sort(key = lambda x:x['f'])	#按每一个活动的结束时间排序
	last_end = 0 #上一个活动的结束时间
	ret = []
	for x in activity:
		if x['s'] >= last_end:
			ret.append(x['no'])
			last_end = x['f']
	return ret


def test_greedy_selector():
	activity = [
		{'no':1,'s':2,'f':3},
		{'no':9,'s':13,'f':17},
		{'no':2,'s':2,'f':4},
		{'no':3,'s':3,'f':5},
		{'no':4,'s':4,'f':7},
		{'no':5,'s':6,'f':8},
		{'no':6,'s':7,'f':11},
		{'no':8,'s':10,'f':15},
		{'no':7,'s':9,'f':12},
	]

	print(greedy_selector(activity))


if __name__ == '__main__':
	test_greedy_selector()