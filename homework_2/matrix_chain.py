#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
给定如下矩阵链：p =（10,100,5,50,30,20,60,45,50）
问题（1）：请写出其最优完全加括号方式
问题（2）：请画出该问题的语法树和相应的最优三角剖分图
'''


def matrix_chain(chain):
    min_value={}
    best_split_pos={}
    n = len(chain)
    for i in range(1,n):
        min_value[i] = {}
        best_split_pos[i] = {}
        min_value[i][i] = 0
        best_split_pos[i][i] = 0
    for l in range(2,n):	#求长度为l的链的最乘积数
        for i in range(1,n - l + 1):	#从 i开始求长度为l的链的最小乘积
            j = i + l - 1		#从i到j的最小乘积数
            min_value[i][j] = min_value[i+1][j] + chain[i - 1] * chain[i] * chain[i + 1] #从i出断开
            best_split_pos[i][j] = i
            for k in range(i+1,j):
                t = min_value[i][k] + min_value[k + 1] [j] + chain[k - 1] * chain[k] * chain[j]
                if t < min_value[i][j] :
                    min_value[i][j] = t
                    best_split_pos[i][j] = k
    return min_value,best_split_pos

def print_table(table):
    print("%s"%" ",end="")
    for i in range(1,len(table) + 1):
        print("\t%8d"%i,end="")
    print()
    for i in range(1,len(table) + 1):
        print("%d"%i,end="")
        for j in range(1,len(table) + 1):
            if j in table[i]:
                print("\t%8d"%table[i][j],end="")
            else:
                print("\t%8s"%" ",end="")
        print()

def test_matrix_chain():
    chain = [10,100,5,50,30,20,60,45,50]
    min_value,best_split_pos = matrix_chain(chain)
    print_table(min_value)
    print_table(best_split_pos)

def best_multiplication():
    def traceback(i,j,best_split_pos):
        if i==j :
            ret = "A%d"%i
            return ret
        ret = "%s%s"%("(",traceback(i,best_split_pos[i][j],best_split_pos))
        ret = "%s%s%s" % (ret, traceback(best_split_pos[i][j] + 1,j, best_split_pos),")")
        return ret
    chain = [10,100,5,50,30,20,60,45,50]
    min_value,best_split_pos = matrix_chain(chain)
    return traceback(1,len(best_split_pos),best_split_pos)



if __name__ == '__main__':
    test_matrix_chain()
    ret = best_multiplication()
    print(ret)
